#!/bin/bash
# Adjust RTC to Localtime #India
sudo timedatectl set-local-rtc 1 --adjust-system-clock

# Switch to fastest mirror, anywhere
sudo sed -i.bak 's|http://in.archive.ubuntu.com/ubuntu/|mirror://mirrors.ubuntu.com/mirrors.txt|g'  /etc/apt/sources.list

# Perform System Update
sudo apt update && sudo apt full-upgrade -y

# Install CURL and aria2
sudo apt install curl aria2 -y

# Download the external packages
aria2c -c -j16 -s16 -x16 -i ubuntu-lts-2004-packagelist.txt

# Enable Spotify
curl -sS https://download.spotify.com/debian/pubkey.gpg | sudo apt-key add - 
echo "deb http://repository.spotify.com stable non-free" | sudo tee /etc/apt/sources.list.d/spotify.list

# Enable Anydesk
wget -qO - https://keys.anydesk.com/repos/DEB-GPG-KEY | sudo apt-key add -
echo "deb http://deb.anydesk.com/ all main" | sudo tee /etc/apt/sources.list.d/anydesk-stable.list

# Enable Riot
wget -qO - https://packages.riot.im/debian/riot-im-archive-keyring.gpg | sudo apt-key add -
echo "deb [arch=amd64] https://packages.riot.im/debian/ $(lsb_release -cs) main" |
    sudo tee /etc/apt/sources.list.d/riot-im.list

# Enable Signal
curl -s https://updates.signal.org/desktop/apt/keys.asc | sudo apt-key add -
  echo "deb https://updates.signal.org/desktop/apt xenial main" | sudo tee -a /etc/apt/sources.list.d/signal-xenial.list

# Enable Jitsi
wget -qO - https://download.jitsi.org/jitsi-key.gpg.key | sudo apt-key add -
sudo sh -c "echo 'deb https://download.jitsi.org stable/' > /etc/apt/sources.list.d/jitsi-stable.list"

# Install Google repo keys
wget -q -O - https://dl.google.com/linux/linux_signing_key.pub | sudo apt-key add -

# Install Essentials
sudo apt install build-essential gnome-tweaks gnome-shell-extensions -y

# Install Extra Utilities
sudo apt install keepassxc numlockx spectre-meltdown-checker inxi hwinfo testdisk pv exfat-fuse exfat-utils cowsay fortune-mod conky hddtemp hdparm smartmontools gparted arj lzip lzop ncompress rzip sharutils unace unar p7zip htop neofetch speedtest-cli android-tools-adb android-tools-fastboot -y

# Install Multimedia
sudo apt install inkscape gimp vlc smplayer mpv smplayer-themes darktable rawtherapee obs-studio audacity blender -y

# Install Communication tools
sudo apt install telegram-desktop signal-desktop riot-web anydesk hexchat -y

# Install local packages
sudo dpkg -i *.deb

# Fix local package install dependencies
sudo apt -fy install

#  Cleanup packages
rm -f *.deb

# Install Extra eye-candy
sudo add-apt-repository ppa:daniruiz/flat-remix -y
sudo apt update
sudo apt install flat-remix flat-remix-gtk flat-remix-gnome -y

# Set the eye candy
gsettings set org.gnome.shell.extensions.user-theme name "Flat-Remix-Dark"
gsettings set org.gnome.desktop.interface gtk-theme "Flat-Remix-GTK-Blue-Dark"
gsettings set org.gnome.desktop.interface icon-theme "Flat-Remix-Blue-Dark"

# Some extra eye candy
sudo apt install numix-blue-gtk-theme numix-icon-theme-circle -y

# Some extra useful utilities
sudo apt install persepolis qbittorrent stellarium wireshark qalculate-gtk aisleriot gnome-chess gnome-mines gnome-mahjongg gnome-sudoku -y

# Bluecurve Mouse Pointers
if [[ -f Bluecurve.tar.xz ]]; then
    sudo tar xvf Bluecurve.tar.xz -C /usr/share/icons
else
    echo "Mouse Pointers Missing ..."
fi

# Extra Fonts for all
if [[ -f restricted-fonts.tar.xz ]]; then
    sudo tar xvf restricted-fonts.tar.xz -C /usr/share/fonts
else
    echo "Restricted Fonts Missing ..."
fi

# Grub Themes
if [[ -d /boot/grub/themes ]]; then
    echo "Themes directory exists. Extracting ..."
else
    echo "Crating directory and extracting ..."
    sudo mkdir /boot/grub/themes
fi

if [[ -f grub-themes.tar.xz ]]; then
    sudo tar xvf grub-themes.tar.xz -C /boot/grub/themes
else
    "Grub Theme Missing ..."
fi

# Android udev rules file
if [[ -f 51-android.rules ]];then
    sudo cp -v 51-android.rules /etc/udev/rules.d/51-android.rules
else
    echo "Android rules file missing ..."
fi

